

function createBall(){
	var divBall = document.createElement("div");
	var random =(Math.floor((Math.random() * 100) + 1));
	divBall.id = "div" + random ;

	divBall.style.background="red";
	divBall.style.position= 'absolute'; 
	divBall.style.height = "40px";
	divBall.style.width = "40px";
	divBall.style.left = Math.floor((Math.random() * window.innerWidth) + 1)+ "px";
	divBall.style.top=Math.floor((Math.random() * window.innerHeight) + 1) + "px";
	divBall.style.color="red";
	divBall.style.borderRadius = "50px";    
	document.body.appendChild(divBall);
	return divBall;
}

var chendu = function(){

	this.ballDiv = createBall();
	this.min_x = Math.random();
	this.min_y = 20;
	this.counter = 1;

	
	this.dx = 20;
	this.dy = 20;
	this.x= 0;
	this.y = 0;
	this.count = 0;

	this.moveBall = function()	{

		this.maxValue_x = window.innerWidth-60;
		this.maxValue_y = window.innerHeight-60;
		this.x+=this.dx;
		this.y+=this.dy;
		this.checkBoundaries();
		this.ballDiv.position="absolute";
		this.ballDiv.style.left = this.x + "px";
		this.ballDiv.style.top  = this.y + "px";
	}

	this.checkBoundaries = function()	{

		if (this.x>this.maxValue_x || this.x<this.min_x )
		{
			this.dx=-this.dx;
		}

		if (this.y> this.maxValue_y || this.y< this.min_y)
		{
			this.dy=-this.dy;
		}

	}
}

var e = document.getElementById('mybtn');
e.onclick = function(){

	var ball=new chendu();
	window.setInterval(function (){ ball.moveBall() },40);

}